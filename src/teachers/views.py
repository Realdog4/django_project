from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import Teacher


def teachers_list(request):
    teachers = Teacher.objects.all()
    return render(request, template_name='teachers_list.html', context={'teachers': teachers})


@csrf_exempt
def teacher_create(request):
    form = """
        <form method="post" >
          <label for="first_name">First name:</label><br>
          <input type="text" id="first_name" name="first_name"><br>

          <label for="last_name">Last name:</label><br>
          <input type="text" id="last_name" name="last_name"><br>

          <label for="email">Email:</label><br>
          <input type="email" id="email" name="email"><br><br>

          <label for="birth_date">Date of Birth:</label><br>
          <input type="date" id="birth_date" name="birth_date"><br>

          <label for="phone_number">Phone number:</label><br>
          <input type="text" id="phone_number" name="phone_number"><br>

          <label for="qualification">Qualification:</label><br>
          <input type="qualification" id="qualification" name="qualification"><br>

          <input type="submit" value="Send">
        </form>"""
    if request.method == "POST":

        Teacher.objects.create(
            first_name=request.POST.get('first_name'),
            last_name=request.POST.get('last_name'),
            email=request.POST.get('email'),
            birth_date=request.POST.get('birth_date'),
            phone_number=request.POST.get('phone_number'),
            qualification=request.POST.get('qualification')
        )
        return HttpResponseRedirect('/teachers')
    elif request.method == "GET":
        pass

    return HttpResponse(form)

