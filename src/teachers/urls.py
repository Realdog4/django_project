from django.urls import path
from teachers.views import teachers_list, teacher_create

urlpatterns = [
    path('', teachers_list, name='teachers_list'),
    path("create/", teacher_create, name="teacher_create"),
]