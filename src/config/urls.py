from django.contrib import admin
from django.urls import path, include

import group.urls
import students.urls
import teachers.urls

urlpatterns = [
    path("__debug__/", include("debug_toolbar.urls")),
    path('admin/', admin.site.urls),
    path('students/', include(students.urls)),
    path('teachers/', include(teachers.urls)),
    path('groups/', include(group.urls)),
]
