from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.models import Student


def index(request):
    return render(
        request,
        template_name="index.html",
        context={"name": "Mykhailo Lazoryk", "list_of_names": ["Anna", "Vitaliy", "Hrygoriy"]},
    )



@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_students(request, params):
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__icontains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})

    return render(request, template_name="students_list.html", context={"students": students})


@csrf_exempt
def create_student(request):
    if request.method == "POST":
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm()
    return render(request, template_name="student_create.html", context={"form": form})


@csrf_exempt
def update_student(request, pk):
    student = get_object_or_404(Student.objects.all(), pk=pk)

    if request.method == "POST":
        form = StudentForm(request.POST, instance=student)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("students:get_students"))
    elif request.method == "GET":
        form = StudentForm(instance=student)

    return render(request, template_name="student_update.html", context={"form": form})
