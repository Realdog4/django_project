from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import Group


def group_list(request):
    groups = Group.objects.all()
    return render(request, template_name='group_list.html', context={'groups': groups})


@csrf_exempt
def group_create(request):
    form = """
        <form method="post" >
          <label for="group_number">Group number:</label><br>
          <input type="number" id="group_number" name="group_number"><br>

          <label for="email_group">Email group:</label><br>
          <input type="email_group" id="emailGroup" name="email_group"><br><br>
          
          <label for="subject">Subject:</label><br>
          <input type="text" id="subject" name="subject"><br><br>
          
          <label for="students_count">Number of students:</label><br>
          <input type="number" id="students_count" name="students_count"><br>
          
          <label for="start_date">Start date:</label><br>
          <input type="date" id="start_date" name="start_date"><br>
          
          <label for="description">Description:</label><br>
          <textarea name="description" placeholder="Your comment" id="description"></textarea>

          <input type="submit" value="Send">
        </form>"""
    if request.method == "POST":

        Group.objects.create(
            group_number=request.POST.get('group_number'),
            email_group=request.POST.get('email_group'),
            subject=request.POST.get('subject'),
            students_count=request.POST.get('students_count'),
            start_date=request.POST.get('start_date'),
            description=request.POST.get('description')
        )
        return HttpResponseRedirect('/groups')
    elif request.method == "GET":
        pass

    return HttpResponse(form)


