from django.urls import path
from group.views import group_list, group_create

urlpatterns = [
    path('', group_list, name='group_list'),
    path("create/", group_create, name="group_create"),
    ]